// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

extern crate diesel;

use std::io::Cursor;
use std::sync::Arc;
use std::time::{Duration, SystemTime};

use chrono::{DateTime, Utc};
use rocket::tokio::sync::Mutex;
use rocket::{
    form::Form,
    fs::FileServer,
    get,
    http::{ContentType, Cookie, CookieJar, SameSite, Status},
    launch, post,
    request::{FromRequest, Outcome, Request},
    response::Redirect,
    routes,
    serde::{Deserialize, Serialize},
    time, uri, State,
};

use diesel::result::{DatabaseErrorKind, Error::DatabaseError};

use rocket_dyn_templates::Template;

use url::Url;

use rand::{distributions::Alphanumeric, Rng};

use reqwest::Client;

type CsrfState = Arc<Mutex<Vec<String>>>;

mod database;
mod oauth;
mod schema;
mod templates;

use database::SoFaDb;
use oauth::OAuth2;

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct OAuthConfig {
    pub client_id: String,
    pub client_secret: String,
    pub gitlab_url: String,
    pub own_base_url: String,
    pub login_description: String,
}

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct EventConfig {
    pub name: String,
    pub description: String,
}

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct OrgaConfig {
    pub members: Vec<String>,
    pub registration_open: bool
}

pub struct AuthenticatedUser {
    token: database::Token,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthenticatedUser {
    type Error = ();

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let cookie = req
            .cookies()
            .get("SOFA-TOKEN")
            .map(|c| c.value().to_owned());
        match cookie {
            Some(token) => {
                let db = SoFaDb::from_request(req)
                    .await
                    .expect("The database fairing should have been already registered");

                db.expire_tokens()
                    .await
                    .expect("Failed to delete outdated tokens");

                match db.verify_token(token.to_string()).await {
                    Ok(Some(token)) => Outcome::Success(AuthenticatedUser { token }),
                    Ok(None) => Outcome::Failure((Status::Unauthorized, ())),
                    Err(error) => {
                        eprintln!("database error: {}", error);
                        Outcome::Failure((Status::InternalServerError, ()))
                    }
                }
            }
            None => Outcome::Failure((Status::Unauthorized, ())),
        }
    }
}

#[allow(dead_code)]
struct OrgaMember {
    user: AuthenticatedUser,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for OrgaMember {
    type Error = ();

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let user = AuthenticatedUser::from_request(req).await;
        if let Outcome::Success(user) = user {
            let orga_members = &req.rocket().state::<OrgaConfig>().unwrap().members;
            if orga_members.contains(&user.token.username) {
                Outcome::Success(OrgaMember { user })
            } else {
                Outcome::Failure((Status::Unauthorized, ()))
            }
        } else {
            Outcome::Failure(user.failed().unwrap())
        }
    }
}

#[get("/")]
async fn index(
    oauth: &State<OAuth2>,
    event_config: &State<OAuthConfig>,
    csrf_tokens: &State<CsrfState>,
) -> Template {
    #[derive(Serialize)]
    #[serde(crate = "rocket::serde")]
    struct Context {
        auth_url: Url,
        login_text: String,
    }

    let (auth_url, csrf_token) = oauth.get_authorization_url();
    csrf_tokens
        .lock()
        .await
        .push(csrf_token.secret().to_string());

    Template::render(
        "index",
        Context {
            auth_url,
            login_text: event_config.login_description.clone(),
        },
    )
}

#[get("/home")]
async fn home(
    user: AuthenticatedUser,
    config: &State<OrgaConfig>,
    event_config: &State<EventConfig>,
) -> Template {
    #[derive(Serialize)]
    #[serde(crate = "rocket::serde")]
    struct Context {
        is_orga_member: bool,
        event_name: String,
        registration_open: bool
    }

    Template::render(
        "home",
        Context {
            is_orga_member: config.members.contains(&user.token.username),
            event_name: event_config.name.clone(),
            registration_open: config.registration_open
        },
    )
}

#[get("/register")]
async fn register(
    user: AuthenticatedUser,
    db: SoFaDb,
    event_config: &State<EventConfig>,
    config: &State<OrgaConfig>
) -> Template {
    let user = db
        .get_user(user.token.username)
        .await
        .expect("Fetch user data");

    #[derive(Serialize)]
    #[serde(crate = "rocket::serde")]
    struct Context {
        username: String,
        email: String,
        event_description: String,
        registration_open: bool
    }

    Template::render(
        "register",
        Context {
            username: user.username,
            email: user.email,
            event_description: event_config.description.clone(),
            registration_open: config.registration_open
        },
    )
}

#[get("/thanks?<already_registered>")]
async fn thanks(_user: AuthenticatedUser, already_registered: bool) -> Template {
    #[derive(Serialize)]
    #[serde(crate = "rocket::serde")]
    struct Context {
        already_registered: bool,
    }

    Template::render("thanks", Context { already_registered })
}

pub fn now_in_unix() -> i64 {
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs()
        .try_into()
        .expect("Value doesn't fit into i64 anymore. Time to upgrade!")
}

#[post("/submit-registration", data = "<registration>")]
async fn submit_registration(
    _user: AuthenticatedUser,
    db: SoFaDb,
    config: &State<OrgaConfig>,
    registration: Form<database::RegistrationForm>,
) -> Redirect {
    if !config.registration_open {
        return Redirect::to(uri!(home))
    }

    let database::RegistrationForm {
        username,
        email,
        alergies,
        name,
    } = registration.into_inner();
    let registration = database::Registration {
        username: Some(username),
        email,
        alergies,
        name,
        datetime: now_in_unix(),
        payed: false,
    };

    let result = db.save_registration(registration).await;
    match result {
        Ok(_) => Redirect::to(uri!(thanks(false))),
        Err(DatabaseError(DatabaseErrorKind::UniqueViolation, _)) => {
            Redirect::to(uri!(thanks(true)))
        }
        Err(err) => Err(err).expect("Failed to save registration"),
    }
}

#[get("/orga")]
async fn orga(_user: OrgaMember, db: SoFaDb) -> Template {
    #[derive(Serialize)]
    #[serde(crate = "rocket::serde")]
    struct Context {
        registrations: Vec<database::Registration>,
        emails: Vec<String>,
        unpaid_emails: Vec<String>,
        paid_emails: Vec<String>,
    }

    let emails = db.get_all_emails().await.expect("Fetching emails");
    let unpaid_emails = db.get_unpaid_emails().await.expect("Fetching emails");
    let paid_emails = db.get_paid_emails().await.expect("Fetching emails");

    let registrations = db
        .get_registrations()
        .await
        .expect("Fetching registrations");

    Template::render(
        "orga",
        Context {
            registrations,
            emails,
            unpaid_emails,
            paid_emails,
        },
    )
}

#[get("/orga/payed?<email>&<payed>")]
async fn set_payed(_user: OrgaMember, db: SoFaDb, email: String, payed: bool) -> Redirect {
    db.set_payed(email, payed).await.expect("Set payed status");

    Redirect::to(uri!(orga))
}

#[get("/orga/delete?<email>")]
async fn delete_registration(_user: OrgaMember, db: SoFaDb, email: String) -> Redirect {
    db.delete(email).await.expect("Delete registration");

    Redirect::to(uri!(orga))
}

#[get("/orga/export")]
async fn export(_user: OrgaMember, db: SoFaDb) -> (ContentType, Vec<u8>) {
    let registrations = db
        .get_registrations()
        .await
        .expect("Getting registrations from the db");

    let mut out = Vec::<u8>::new();

    {
        let mut writer = csv::Writer::from_writer(Cursor::new(&mut out));

        for registration in registrations {
            writer
                .write_record(&[
                    registration.name,
                    registration.username.unwrap_or(String::new()),
                    registration.email,
                    registration.alergies,
                    DateTime::<Utc>::from(
                        SystemTime::UNIX_EPOCH + Duration::from_secs(registration.datetime as u64),
                    )
                    .format("%d %b %Y %H:%M %Z")
                    .to_string(),
                    if registration.payed {
                        "bezahlt".to_string()
                    } else {
                        "nicht bezahlt".to_string()
                    },
                ])
                .expect("Failed to write to csv");
        }
    }

    (ContentType::CSV, out)
}

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
struct GitlabUser {
    username: String,
    email: String,
}

#[get("/authorize?<code>&<state>")]
async fn authorize(
    oauth: &State<OAuth2>,
    http_client: &State<Client>,
    oauth_config: &State<OAuthConfig>,
    csrf_tokens: &State<CsrfState>,
    cookies: &CookieJar<'_>,
    db: SoFaDb,
    code: String,
    state: String,
) -> Redirect {
    if !csrf_tokens.lock().await.contains(&state) {
        return Redirect::to(uri!(index));
    }

    csrf_tokens.lock().await.retain(|e| e != &state);

    let access_token = oauth.oauth_authorize(code).await.unwrap();

    let gitlab_user = http_client
        .get(format!("{}/api/v4/user", oauth_config.gitlab_url))
        .header(
            "Authorization",
            &format!("Bearer {}", access_token.secret()),
        )
        .send()
        .await
        .expect("Fetching user info")
        .json::<GitlabUser>()
        .await
        .expect("Parsing user info");

    db.add_user(database::User {
        username: gitlab_user.username.clone(),
        email: gitlab_user.email,
    })
    .await
    .expect("Storing user");
    let token: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(20)
        .map(char::from)
        .collect();
    db.add_token(database::Token {
        username: gitlab_user.username,
        token: token.clone(),
        timestamp: now_in_unix(),
    })
    .await
    .expect("Storing user token");

    let mut cookie = Cookie::new("SOFA-TOKEN", token);
    // TODO why do the defaults work for matepay????
    cookie.set_same_site(Some(SameSite::Lax));
    cookie.set_secure(true);
    cookie.set_http_only(true);
    cookie.set_expires(time::OffsetDateTime::now_utc() + time::Duration::days(1));
    cookies.add(cookie);

    Redirect::to(uri!(home))
}

#[get("/privacy")]
async fn privacy() -> Template {
    #[derive(Serialize)]
    #[serde(crate = "rocket::serde")]
    struct Context {}
    Template::render("privacy", Context {})
}

#[launch]
fn rocket() -> _ {
    let rocket = rocket::build();
    let figment = rocket.figment();
    let oauth_config: OAuthConfig = figment
        .extract_inner("oauth")
        .expect("Reading oauth config");
    let orga_config: OrgaConfig = figment.extract_inner("orga").expect("Reading orga config");
    let event_config: EventConfig = figment
        .extract_inner("event")
        .expect("Reading event config");

    let csrf_tokens: CsrfState = Arc::new(Mutex::new(Vec::new()));

    rocket
        .manage(oauth::OAuth2::new(&oauth_config).unwrap())
        .manage(Client::new())
        .manage(oauth_config)
        .manage(orga_config)
        .manage(event_config)
        .manage(csrf_tokens)
        .mount(
            "/",
            routes![
                index,
                privacy,
                authorize,
                home,
                register,
                thanks,
                submit_registration,
                orga,
                set_payed,
                delete_registration,
                export
            ],
        )
        .attach(templates::TemplateExtensions::fairing())
        .attach(SoFaDb::fairing())
        .mount("/css", FileServer::from("css"))
        .mount("/img", FileServer::from("img"))
}
