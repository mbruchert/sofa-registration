// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use crate::{now_in_unix, schema::*};

use rocket::serde::Serialize;
use rocket::FromForm;
use rocket_sync_db_pools::{database, diesel};

use diesel::dsl::count;
use diesel::prelude::*;

#[database("sofa")]
pub struct SoFaDb(diesel::SqliteConnection);

#[derive(Insertable, Queryable)]
pub struct User {
    pub username: String,
    pub email: String,
}

#[derive(Insertable, Queryable)]
pub struct Token {
    pub username: String,
    pub token: String,
    pub timestamp: i64,
}

#[derive(FromForm)]
pub struct RegistrationForm {
    pub username: String,
    pub name: String,
    pub email: String,
    pub alergies: String,
}

#[derive(Insertable, Queryable, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct Registration {
    pub username: Option<String>,
    pub name: String,
    pub email: String,
    pub alergies: String,
    pub datetime: i64,
    pub payed: bool,
}

impl SoFaDb {
    pub async fn add_user(&self, user: User) -> QueryResult<()> {
        self.run(move |c| -> QueryResult<()> {
            let exists = users::table
                .filter(users::username.eq(&user.username))
                .select(count(users::username).gt(0))
                .get_result::<bool>(c)?;
            if !exists {
                diesel::insert_into(users::table).values(user).execute(c)?;
            }
            Ok(())
        })
        .await?;
        Ok(())
    }

    pub async fn get_user(&self, username: String) -> QueryResult<User> {
        self.run(move |c| {
            users::table
                .filter(users::username.eq(username))
                .get_result(c)
        })
        .await
    }

    pub async fn add_token(&self, token: Token) -> QueryResult<()> {
        self.run(move |c| diesel::insert_into(tokens::table).values(token).execute(c))
            .await?;
        Ok(())
    }

    pub async fn verify_token(&self, token: String) -> QueryResult<Option<Token>> {
        self.run(move |c| {
            tokens::table
                .filter(tokens::token.eq(token))
                .get_result(c)
                .optional()
        })
        .await
    }

    pub async fn save_registration(&self, registration: Registration) -> QueryResult<()> {
        self.run(move |c| {
            diesel::insert_into(registrations::table)
                .values(registration)
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn get_registrations(&self) -> QueryResult<Vec<Registration>> {
        self.run(move |c| {
            registrations::table
                .order_by(registrations::datetime.asc())
                .get_results(c)
        })
        .await
    }

    pub async fn set_payed(&self, email: String, payed: bool) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(registrations::table)
                .filter(registrations::email.eq(email))
                .set(registrations::payed.eq(payed))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn delete(&self, email: String) -> QueryResult<()> {
        self.run(move |c| {
            diesel::delete(registrations::table)
                .filter(registrations::email.eq(email))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn expire_tokens(&self) -> QueryResult<()> {
        self.run(move |c| {
            let now = now_in_unix();
            // This is a bit ugly, but I can't find an abs function or way to substract timestamp from now in diesel.
            // So the result will always be negative. If it is less than minus a day, the token has been created more than a day ago.
            diesel::delete(tokens::table.filter((tokens::timestamp - now).lt(-24 * 60 * 60)))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn get_all_emails(&self) -> QueryResult<Vec<String>> {
        self.run(move |c| registrations::table.select(registrations::email).load(c))
            .await
    }

    pub async fn get_unpaid_emails(&self) -> QueryResult<Vec<String>> {
        self.run(move |c| {
            registrations::table
                .filter(registrations::payed.eq(false))
                .select(registrations::email)
                .load(c)
        })
        .await
    }

    pub async fn get_paid_emails(&self) -> QueryResult<Vec<String>> {
        self.run(move |c| {
            registrations::table
                .filter(registrations::payed.eq(true))
                .select(registrations::email)
                .load(c)
        })
        .await
    }
}
